﻿// MyQuickNote.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyQuickNote.h"
#include <windowsX.h>
#include <winuser.h>
#include <commctrl.h>
#include <iostream>
#include "shellapi.h"
#include <fstream>
#include <locale>
#include <fcntl.h>
#include <codecvt>>
#include "commctrl.h"
#include "list"
#include <vector>
#define D_SCL_SECURE_NO_WARNINGS
#pragma warning(disable:4996)
using namespace std;


#ifdef _MSC_VER
#pragma warning(pop)
#endif

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

#define MAX_LOADSTRING 100
#define WM_MYMESSAGE (WM_USER + 1)
#define POPUP_VIEW 1
#define POPUP_STATIC 2
#define POPUP_EXIT 3
#define IDC_LISTVIEW 100002
#define IDM_VIEWSTATITISTICS 1000
#define IDM_VIEWNOTE 2000
//cấu trúc dữ liệu
struct myNote
{
	wstring Tag;
	wstring	Note;
	wstring Date;
	int ID;
};

struct listTag
{
	wstring Tag;
	vector<int> ListID;
};


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR *buffer1;
WCHAR *buffer2;
HHOOK hHook = NULL;
HINSTANCE hinstLib;
HWND hWnd = NULL;
HWND hTableList, hTreeViewList1, hTreeViewList2;
bool isRun = false;
HWND hwndGotoDialog = NULL;
static HWND listview1= NULL;
HWND hwnddl;
HWND hwndchart;
NOTIFYICONDATA nid;
HMENU hPopMenu;



vector <myNote>  _myNote;
vector <listTag> _listTag;
int countNote = 0;
static wfstream file;

// Forward declarations of functions included in this code module
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	AddNoteDialog_WndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	KeyBoardHookProc(int nCode, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK DetailProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK DetailChart(HWND hwndchart, UINT message, WPARAM wParam, LPARAM lParam);

BOOL InsertListViewItems(HWND hWndListView, vector<myNote> _myNote); //thêm danh sách
BOOL InsertListViewItems(HWND hWndListView, wstring toString); //thêm chuỗi
BOOL InitListViewColumns(HWND hWndListView, list <WCHAR*> szText);
HWND CreateATreeView(HWND hwndParent, int x, int y, int witdh, int height);
HWND CreateListView(HWND hwndParent, int x, int y, int witdh, int height);

LPWSTR ConvertToLPWSTR(const std::string& s);
bool keyPress(int a);
void saveNote(WCHAR* tag, WCHAR* note);
void loadNote(myNote *a, int &countNote);
void loadToTreeView(HWND m_hTreeView);
void cutTag(vector<myNote> _myNote, vector<listTag> &_listTag);
vector<wstring> Cut(wstring data);
wstring DeleteSpace(wstring in);
int isExist(vector<listTag> arr, wstring a);


list <WCHAR*> szText = { L"Date",L"Tag",L"Note" };
list <WCHAR*> Col={ L"Sort of Date" };
//bắt đầu code
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_MYQUICKNOTE, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYQUICKNOTE));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}




ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYQUICKNOTE));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MYQUICKNOTE);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{

	 hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }
  
   nid.cbSize = sizeof(NOTIFYICONDATA);
   nid.hWnd = hWnd;
   nid.uID = 100;
   nid.uVersion = NOTIFYICON_VERSION;
   nid.uCallbackMessage = WM_MYMESSAGE;
   nid.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYQUICKNOTE));
   wcscpy_s(nid.szTip, L"Quick Note");
   nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
   Shell_NotifyIcon(NIM_ADD, &nid);

   //ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	POINT lpClickPoint;
	switch (message)
	{
	case  WM_MYMESSAGE:
		// systray msg callback 
		switch (LOWORD(lParam))
		{
		case WM_RBUTTONDOWN:
		{
			UINT uFlag = MF_BYPOSITION | MF_STRING;
			GetCursorPos(&lpClickPoint);
			hPopMenu = CreatePopupMenu();
			InsertMenu(hPopMenu, 0, MF_BYPOSITION | MF_STRING, IDM_VIEWNOTE, _T("View Notes"));
			InsertMenu(hPopMenu, 0, uFlag, IDM_VIEWSTATITISTICS, _T("View Statitistics"));
			InsertMenu(hPopMenu, 0, MF_BYPOSITION | MF_STRING, IDM_EXIT, _T("Exit"));
			SetForegroundWindow(hWnd);
			TrackPopupMenu(hPopMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_BOTTOMALIGN, lpClickPoint.x, lpClickPoint.y, 0, hWnd, NULL);
			return TRUE;
		}
		case WM_LBUTTONDBLCLK:
		{
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOGADDNOTE), hWnd, AddNoteDialog_WndProc);
			return TRUE;
		}
		}
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_VIEWNOTE:
			//DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOGVIEWNOTE), NULL, (DLGPROC)DetailProc);
			ShowWindow(CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOGVIEWNOTE), hWnd, (DLGPROC)DetailProc), 1);
			break;
		case IDM_VIEWSTATITISTICS:
			DialogBox(hInst, (LPCTSTR)IDD_DIALOGCHART, NULL, (DLGPROC)DetailChart);
			break;
		case IDM_EXIT:
			Shell_NotifyIcon(NIM_DELETE, &nid);
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	
	case WM_DESTROY:
		DialogBox(hInst, (LPCTSTR)IDD_DIALOGCHART, NULL, (DLGPROC)DetailChart);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}



//void doProcessPopup(HMENU hMenu, HWND hDlg,LPARAM lParam)
//{
//	POINT point;
//	point.x = GET_X_LPARAM(lParam); // Tránh dùng LOWORD(lParam), ko đúng với nhiều màn hình
//	point.y = GET_Y_LPARAM(lParam);
//
//	int res = TrackPopupMenu(hMenu, TPM_RETURNCMD | TPM_TOPALIGN | TPM_LEFTALIGN, point.x, point.y, 0, hDlg, 0);
//	switch (res)
//	{
//	case POPUP_VIEW:
//		DialogBox(hInst, (LPCTSTR)IDD_DIALOGVIEWNOTE, NULL, (DLGPROC)DetailProc);
//		
//		break;
//	case POPUP_STATIC:
//		DialogBox(hInst, (LPCTSTR)IDD_DIALOGCHART, NULL, (DLGPROC)DetailChart);
//		//MessageBox(0, L"Static", L"Static", 0);
//		break;
//	case POPUP_EXIT:
//		Shell_NotifyIcon(NIM_DELETE, &nid); //Xóa icon
//		MessageBox(NULL, L"Sau thông báo này icon ở khay hệ thống sẽ hiện lại!", L"Thông báo", MB_OK); 
//		Shell_NotifyIcon(NIM_ADD, &nid); //thêm lại icon
//		break;
//	}
//}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


bool keyPress(int a)
{
	if (GetAsyncKeyState(a))
		return true;
	return false;
}

INT_PTR CALLBACK AddNoteDialog_WndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	int count;
	static HMENU hMenu = NULL;
	switch (message)
	{
	
	case WM_MYMESSAGE:
		switch (lParam)
		{
		case WM_LBUTTONDBLCLK:
		{
			hwndGotoDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOGADDNOTE), hWnd, (DLGPROC)AddNoteDialog_WndProc);
			ShowWindow(hwndGotoDialog, 1);
			break;
		}
		default:
			break;
		}
	case WM_COMMAND:
		int id = LOWORD(wParam);
		switch (id) {
		case ID_SAVE:
		{
			int max1 = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT1)) + 1;
			int max2 = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT2)) + 1;
			if (max1 == 1)
			{
				MessageBox(0, L"Your tag is emty!", L"Warning!", 0); break;
			}
			if (max2 == 1)
			{
				MessageBox(0, L"Your note is emty!", L"Warning!", 0); break;
			}
			int check = MessageBox(0, L"Do you want to add new Note?", L"Save", MB_YESNOCANCEL);
			if (check==IDYES)
			{
				buffer1 = new WCHAR[max1];
				buffer2 = new WCHAR[max2];
				GetDlgItemText(hDlg, IDC_EDIT1, buffer1, max1);
				GetDlgItemText(hDlg, IDC_EDIT2, buffer2, max2);
				saveNote(buffer1, buffer2);
				EndDialog(hDlg, TRUE);
				return (INT_PTR)TRUE;
			}
			if (check == IDNO)
			{
				EndDialog(hDlg, TRUE);
				return (INT_PTR)TRUE;
			}
		}
			break;
		case ID_CANCEL:
			int max1 = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT1)) + 1;
			int max2 = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT2)) + 1;
			if (max1 == 1 && max2 == 1)
			{
				EndDialog(hDlg, FALSE);
				return (INT_PTR)TRUE;
			}
			else
				if (MessageBox(0, L"Your note won't save. Do you want continue?", L"Warnig!", MB_YESNO) == IDYES)
				{
					EndDialog(hDlg, FALSE);
					return (INT_PTR)TRUE;
				}
			break;
		}
	}
	return (INT_PTR)FALSE;
}

void saveNote(WCHAR* tag, WCHAR* note)
{
	const locale utf8_locale = locale(locale(), new codecvt_utf8<TCHAR>());
	file.open(L"save.txt");
	file.imbue(utf8_locale);

	file.seekp(0, file.end);
	file << "\n"<<tag << "\n";
	file << note << "\n";

	SYSTEMTIME st = { 0 };
	::GetLocalTime(&st);
	WCHAR *time = new WCHAR[30];
	swprintf(time, 30, L"%02d/%02d/%02d %02d:%02d", st.wDay, st.wMonth, st.wYear,st.wHour,st.wMinute);
	file << time ;

	file.seekp(0, file.beg);
	file.close();
	MessageBox(0, L"Your note saved!", L"Saved", 0);
}

LRESULT CALLBACK KeyBoardHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	
	if (nCode == HC_ACTION)
	{
		switch (wParam)
		{
		case WM_KEYDOWN:
			break;
		case WM_KEYUP:
		{
			if ((lParam & (1 << 30)) == 0)
			{
				if (GetKeyState(VK_CONTROL) & 0x80
					&& GetKeyState(VK_SPACE) & 0x80)
				{
					ShowWindow(hwndGotoDialog, 1);
				}
			}
		}
		break;

		}
	}
	return true;
}

void loadNote(vector<myNote> &_myNote)
{
	_myNote.clear();
	myNote BufferAdd;
	countNote = 0;
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<TCHAR>());
	wifstream file;
	file.imbue(utf8_locale);
	file.open("save.txt");
	getline(file, BufferAdd.Tag, L'\n');
	for (int i = 0; file.eof() == false;)
	{
		getline(file, BufferAdd.Tag, L'\n');
		getline(file, BufferAdd.Note, L'\n');
		getline(file, BufferAdd.Date, L'\n');
		BufferAdd.ID = countNote;
		_myNote.push_back(BufferAdd);
		countNote++;
	}
	file.close();
}

INT_PTR CALLBACK DetailProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	int iIndex;
	LPNMLISTVIEW pnm;
	TCHAR *pVarName = NULL;
	POINT pt;
	static RECT lstRect;

	switch (message)
	{
	case WM_INITDIALOG:
	{
		GetWindowRect(hDlg, &lstRect);
		hTableList = CreateListView(hDlg, 10, 40, 575, 160);
		InitListViewColumns(hTableList, szText);
		loadNote(_myNote);
		InsertListViewItems(hTableList, _myNote);

		_listTag.clear();
		hTreeViewList1 = CreateATreeView(hDlg, 10, 260, 250, 160);
		cutTag(_myNote, _listTag);
		loadToTreeView(hTreeViewList1);

		hTreeViewList2 = CreateListView(hDlg, 310, 260, 270, 160);
		InitListViewColumns(hTreeViewList2, Col);
		return (INT_PTR)TRUE;
	}
	//break;
	case WM_COMMAND:
	{

		if (LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	case WM_NOTIFY:
	{
		if ((((LPNMHDR)lParam)->hwndFrom) == hTreeViewList1)
		{
			switch (((LPNMHDR)lParam)->code)
			{

			case TVN_SELCHANGED:
			{
				ListView_DeleteAllItems(hTreeViewList2);
				HTREEITEM hSelectedItem = TreeView_GetSelection(hTreeViewList1);

				// Now get the text of the selected item
				WCHAR buffer[2048];

				TVITEM item;
				item.hItem = hSelectedItem;
				item.mask = TVIF_TEXT;
				item.cchTextMax = 2048;
				item.pszText = buffer;
				if (TreeView_GetItem(hTreeViewList1, &item))
				{
					wstring toString = item.pszText;
					toString.erase(toString.size() - 4, toString.size());
					InsertListViewItems(hTreeViewList2, toString);
				}

			}
			//break;


			}
		}
		if ((((LPNMHDR)lParam)->hwndFrom) == hTreeViewList2)
		{
			switch (((LPNMHDR)lParam)->code)
			{

			case NM_DBLCLK:
			{
				int iPos = ListView_GetNextItem(hTreeViewList2, -1, LVNI_SELECTED);
				while (iPos != -1)
				{
					WCHAR pszText[256] = { 0 };
					ListView_GetItemText(hTreeViewList2, iPos, 0, pszText, 255);

					LVITEM lv;

					lv.mask = LVIF_TEXT;
					lv.iSubItem = 0;
					lv.iItem = iPos;
					lv.pszText = pszText;
					lv.cchTextMax = 255;


					MessageBox(hDlg, (LPCWSTR)lv.pszText, L"Content Note", 0);
					break;
				}
			}
			break;
			}
		}

	return (INT_PTR)TRUE;
	}
	case WM_DESTROY:
		DialogBox(hInst, (LPCTSTR)IDD_DIALOGCHART, NULL, (DLGPROC)DetailChart);
		PostQuitMessage(0);
		break;
	}

	return (INT_PTR)FALSE;
}
HWND CreateATreeView(HWND hwndParent, int x, int y, int witdh, int height)
{
	HWND hwndTV;   
	InitCommonControls();
	hwndTV = CreateWindowEx(0,WC_TREEVIEW,TEXT(""),WS_VISIBLE | WS_CHILD | WS_BORDER | TVS_HASLINES,x,y,witdh,height,hwndParent,NULL,hInst,NULL);
	return hwndTV;
}
void cutTag(vector<myNote> _myNote, vector<listTag> &_listTag)
{
	_listTag.clear();
	for (int i = 0; i < _myNote.size(); i++)
	{
		vector<wstring> TagToken = Cut(_myNote[i].Tag);

		for (int j = 0; j < TagToken.size(); j++)
		{
			int pos = isExist(_listTag, TagToken[j]);
			if (pos == -1)
			{
				listTag tmp;
				tmp.ListID.push_back(_myNote[i].ID);
				tmp.Tag = TagToken[j];

				_listTag.push_back(tmp);
			}
			else
			{
				_listTag[pos].ListID.push_back(_myNote[i].ID);
			}
		}
	}
}
int isExist(vector<listTag> arr, wstring a)
{
	for (int i = 0; i < arr.size(); i++)
	{
		if (a == arr[i].Tag)
			return i;
	}
	return -1;
}
vector<wstring> Cut(wstring data)
{
	vector<wstring> rls;
	wstring tmp = data;
	wstring::size_type found;
	while (tmp != L"")
	{
		found = tmp.find_first_of(L",");
		if (found != wstring::npos)
		{
			wstring tok(tmp.begin(), tmp.begin() + found);

			//Xóa dấu cách
			tok = DeleteSpace(tok);

			rls.push_back(tok);

			tmp.erase(0, found + 1);
		}
		else
		{
			wstring tok(tmp);

			//Xóa dấu cách
			tok = DeleteSpace(tok);

			rls.push_back(tok);
			tmp.clear();
		}
	}
	return rls;
}
wstring DeleteSpace(wstring in)
{
	while (in[0] == L' ')
	{
		in.erase(0, 1);
	}
	while (in[in.size() - 1] == L' ')
	{
		in.erase(in.size() - 2, in.size() - 1);

	}
	return in;
}
BOOL InsertListViewItems(HWND hWndListView, vector<myNote> _myNote)
{
	LVITEM lvI;
	for (int index = 0; index < _myNote.size(); index++)
	{
		myNote tmp;

		tmp.Date = _myNote[index].Date;
		tmp.Note = _myNote[index].Note;
		tmp.Tag = _myNote[index].Tag;

		lvI.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lvI.iItem = index;

		lvI.iSubItem = 0;
		lvI.pszText = &tmp.Date[0];
		lvI.state = 0;
		ListView_InsertItem(hWndListView, &lvI);

		lvI.mask = LVIF_TEXT;

		lvI.iSubItem = 1;
		lvI.pszText = &tmp.Tag[0];
		ListView_SetItem(hWndListView, &lvI);

		lvI.iSubItem = 2;
		lvI.pszText = &tmp.Note[0];
		ListView_SetItem(hWndListView, &lvI);
	}

	return TRUE;
}

HWND CreateListView(HWND hwndParent, int x, int y, int witdh, int height)
{
	INITCOMMONCONTROLSEX icex;           // Structure for control initialization.
	icex.dwICC = ICC_LISTVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	// Create the list-view window in report view with label editing enabled.
	HWND hWndListView = CreateWindow(WC_LISTVIEW,
		L"",
		WS_CHILD | WS_BORDER | LVS_REPORT | LVS_EDITLABELS | WS_VISIBLE | LVS_REPORT | LVS_EDITLABELS | LVS_ICON | LVS_SHOWSELALWAYS,
		x, y, witdh, height,
		hwndParent,
		NULL,
		hInst,
		NULL);

	return (hWndListView);
}


BOOL InitListViewColumns(HWND hWndListView, list <WCHAR*> szText)
{
	RECT rect;
	GetWindowRect(hWndListView, &rect);

	//WCHAR szText[2][256] = { L"Tag", L"S? lý?ng" };     // Temporary buffer.
	LVCOLUMN lvc;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	// Add the columns.
	int pos = 0;
	for (list<WCHAR*>::iterator iCol = szText.begin(); iCol != szText.end(); iCol++)
	{
		lvc.iSubItem = pos;
		lvc.pszText = *iCol;
		if (szText.size() >= 2)
		{
			lvc.cx = (rect.right - rect.left - 100) / (szText.size() - 1);            // Width of column in pixels.
			if (iCol == szText.begin())
			{
				lvc.cx = 100;
				lvc.fmt = LVCFMT_CENTER;
			}
		}
		else
			lvc.cx = rect.right - rect.left;

		// Load the names of the column headings from the string resources.
		//LoadString(hInst, NULL, szText[iCol], sizeof(szText) / sizeof(szText[0]));

		// Insert the columns into the list view.
		if (ListView_InsertColumn(hWndListView, pos, &lvc) == -1)
			return FALSE;
		pos++;
	}
	return TRUE;
}
void loadToTreeView(HWND m_hTreeView)
{

	TV_INSERTSTRUCT tvInsert;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;

	for (int i = 0; i < _listTag.size(); i++) {
		tvInsert.hParent = NULL;
		tvInsert.hInsertAfter = TVI_LAST;
		wstring name = _listTag[i].Tag + L" (" + to_wstring(_listTag[i].ListID.size()) + L")";
		tvInsert.item.pszText = (LPWSTR)name.c_str();
		HTREEITEM hDesktop = TreeView_InsertItem(m_hTreeView, &tvInsert);
	}
}
LPWSTR ConvertToLPWSTR(const std::string& s)
{
	LPWSTR ws = new wchar_t[s.size() + 1]; // +1 for zero at the end
	copy(s.begin(), s.end(), ws);
	ws[s.size()] = 0; // zero at the end
	return ws;
}
LRESULT CALLBACK DetailChart(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	loadNote(_myNote);
	cutTag(_myNote, _listTag);
	int iIndex;
	LPNMLISTVIEW pnm;
	TCHAR *pVarName = NULL;
	POINT pt;
	static RECT lstRect;

	switch (message)
	{
	case WM_INITDIALOG:
		//hwnddl = CreateListView(hDlg, 10, 10, 200, 200);
		//InitListViewColumns(hwnddl, szText);
		break;
	case WM_PAINT:
	{
		struct TOP5 {
			wstring Tag;
			int Num;
		};
		TOP5 buf = { L"",0 };
		TOP5 top[5] = { buf,buf,buf,buf,buf };
		listTag swag;
		for (int i = 0;i < _listTag.size();i++)
		{
			for (int j = i;j < _listTag.size();j++)
			{
				if (_listTag[i].ListID.size() < _listTag[j].ListID.size())
				{
					swag = _listTag[i];
					_listTag[i] = _listTag[j];
					_listTag[j] = swag;
				}
			}
		}
		for (int i = 0;i < _listTag.size();i++)
		{
			top[i].Tag = _listTag[i].Tag;
			top[i].Num = _listTag[i].ListID.size();
		}
		float x;
		int Sum = top[0].Num + top[1].Num + top[2].Num + top[3].Num + top[4].Num;
		//TOP top5[5];
		PAINTSTRUCT PaintStruct;
		HDC hDC;
		HBRUSH      Red, Blue, Yellow, Green, Gray;
		Red = CreateSolidBrush(RGB(250, 25, 5));
		Blue = CreateSolidBrush(RGB(0, 0, 255));
		Gray = CreateSolidBrush(RGB(190, 190, 190));
		Green = CreateSolidBrush(RGB(0, 255, 0));
		Yellow = CreateSolidBrush(RGB(255, 255, 0));
		hDC = BeginPaint(hDlg, &PaintStruct);
		SelectObject(hDC, CreateSolidBrush(RGB(250, 25, 5)));
		
		
		x = 220 - (top[0].Num/float(Sum)) * 200;
		Rectangle(hDC, 20, 220, 40, x);
		//LPWSTR pcstr = ConvertToLPWSTR(top[0].Tag);
		TextOutW(hDC, 20, 230, top[0].Tag.c_str(), 3);
		wchar_t buffer1[256];
		wsprintfW(buffer1, L"%d", top[0].Num);
		TextOutW(hDC, 27, x-20, buffer1, 1);

		wsprintfW(buffer1, L"%d", Sum);
		TextOutW(hDC, 17,35 , buffer1, 2);

		SelectObject(hDC, CreateSolidBrush(RGB(0, 0, 255)));
		x = 220 - (top[1].Num / float(Sum)) * 200;
		Rectangle(hDC, 60, 220, 80, x);
		TextOutW(hDC, 60, 230, top[1].Tag.c_str(), 3);
		wsprintfW(buffer1, L"%d", top[1].Num);
		TextOutW(hDC, 67, x - 20, buffer1, 1);

		SelectObject(hDC, CreateSolidBrush(RGB(190, 190, 190)));
		x = 220 - (top[2].Num / float(Sum)) * 200;
		Rectangle(hDC, 100, 220, 120, x);
		TextOutW(hDC, 100, 230, top[2].Tag.c_str(), 3);
		wsprintfW(buffer1, L"%d", top[2].Num);
		TextOutW(hDC, 107, x - 20, buffer1, 1);

		SelectObject(hDC, CreateSolidBrush(RGB(0, 255, 0)));
		x = 220 - (top[3].Num / float(Sum)) * 200;
		Rectangle(hDC, 140, 220, 160, x);
		TextOutW(hDC, 140, 230, top[3].Tag.c_str(), 3);
		wsprintfW(buffer1, L"%d", top[3].Num);
		TextOutW(hDC, 147, x - 20, buffer1, 1);

		SelectObject(hDC, CreateSolidBrush(RGB(255, 255, 0)));
		x = 220 - (top[4].Num / float(Sum)) * 200;
		Rectangle(hDC, 180, 220, 200, x);
		TextOutW(hDC, 180, 230, top[4].Tag.c_str(), 3);
		wsprintfW(buffer1, L"%d", top[4].Num);
		TextOutW(hDC, 187, x - 20, buffer1, 1);
		//	Rectangle(hDC, 300, 200, 300, 300);

		EndPaint(hDlg, &PaintStruct);
		break;
	}
	case WM_COMMAND:
	{
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	}

	return 0;
}
BOOL InsertListViewItems(HWND hWndListView, wstring toString)
{
	int pos = -1;
	vector <wstring> temp;
	for (int i = 0; i < _listTag.size(); i++)
	{
		 pos = isExist(_listTag, toString);
		 if (pos > -1) {
			 for (int j = 0; j < _listTag[pos].ListID.size();j++)
			 {
				 int index = _listTag[pos].ListID[j];
				 temp.push_back(_myNote[index].Note);
			 }
			 break;
		 }
	}

	LVITEM lvI;
	for (int i = 0; i < temp.size(); i++)
	{
		lvI.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lvI.iItem = 0;

		lvI.iSubItem = 0;
		lvI.pszText = &temp[i][0];
		lvI.state = 0;
		ListView_InsertItem(hTreeViewList2, &lvI);

	}
	
	return true;
}